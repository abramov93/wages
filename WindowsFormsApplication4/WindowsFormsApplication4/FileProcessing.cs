﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    class FileProcessing
    {
        /// <summary>
        /// Ссылка на объект для чтения данных из текстового файла
        /// </summary>
        StreamReader sr;

        /// <summary>
        /// Метод, организующий запись коллекции работников в текстовый файл
        /// </summary>
        /// <param name="workers"></param>
        /// <param name="pathToTextFile"></param>
        public void WriteToFile(List<Workers> workers, string pathToTextFile)
        {
            StreamWriter sw = new StreamWriter(pathToTextFile);
      
            for (int i = 0; i < workers.Count; i++)
            {
                sw.WriteLine("{0,-8} {1,-12} {2,-12} {3, -50}", workers[i].Id, workers[i].Name, workers[i].AverageMonthlyWage, workers[i].ToString());
            }

            sw.Close();
        }

        /// <summary>
        /// Метод, организующий чтение коллекции работников из текстового файла
        /// </summary>
        /// <param name="workers"></param>
        /// <param name="pathToTextFile"></param>
        public void ReadFromFile(ref List<Workers> workers, ref string pathToTextFile)
        {
            try
            {
                sr = new StreamReader(pathToTextFile);
            }
            catch
            {
                MessageBox.Show("Файл '" + pathToTextFile + "' не найден!");
                return;
            }

            string allReadText = sr.ReadToEnd();

            string[] textFileWords;
            
            string[] stringSeparators = new string[] { " " };

            textFileWords = allReadText.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

            workers = new List<Workers>();

            CreateParseWorkers(workers, pathToTextFile, textFileWords);

            sr.Close();
        }

        /// <summary>
        /// Создание объектов коллекции parseWorkers соответствующих классов из текстового документа c присвоением полей
        /// </summary>
        private void CreateParseWorkers(List<Workers> workers, string pathToTextFile, string[] textFileWords)
        {
            for (int i = 0, j = 3; i < textFileWords.Length / 4; i++, j = j + 4)
            {
                if (textFileWords[j] == "WindowsFormsApplication4.FixedWageWorkers")
                    workers.Add(new FixedWageWorkers());
                else if (textFileWords[j] == "WindowsFormsApplication4.HourlyWageWorkers")
                    workers.Add(new HourlyWageWorkers());
                else
                {
                    MessageBox.Show("Текстовый файл '" + pathToTextFile + "' содержит объекты неизвестного типа!");
                    return;
                }
            }

            try
            {
                for (int i = 0, j = 0; i < workers.Count; i++, j = j + 2)
                {
                    workers[i].Id = Convert.ToInt32(textFileWords[j]);
                    workers[i].Name = textFileWords[++j];
                    workers[i].AverageMonthlyWage = Convert.ToDouble(textFileWords[++j]);
                }
            }

            catch
            {
                MessageBox.Show("Текстовый файл '" + pathToTextFile + "' содержит некорректные данные!");
                return;
            }
        }
    }
}
