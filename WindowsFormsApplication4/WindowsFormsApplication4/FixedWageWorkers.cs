﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication4
{
    /// <summary>
    /// Класс, описывающий информацию о работниках с фиксированной оплатой
    /// </summary>
    public class FixedWageWorkers : Workers
    {
        /// <summary>
        /// Фиксированная месячная оплата
        /// </summary>
        double wage;

        public FixedWageWorkers() { }

        public FixedWageWorkers(double wage)
        {
            this.wage = wage;
        }

        public override double WageCalculate()
        {
            return wage;
        }
    }
}
