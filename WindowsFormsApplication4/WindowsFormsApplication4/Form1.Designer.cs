﻿namespace WindowsFormsApplication4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.workerId_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workerName_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workerPay_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.workerId_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workerName_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workerPay_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.workerId_3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workerName_3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workerPay_3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.showParseWorkers_Button = new System.Windows.Forms.Button();
            this.read_Button = new System.Windows.Forms.Button();
            this.write_Button = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(414, 306);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(406, 280);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "List of workers";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.workerId_1,
            this.workerName_1,
            this.workerPay_1});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(400, 274);
            this.dataGridView1.TabIndex = 0;
            // 
            // workerId_1
            // 
            this.workerId_1.HeaderText = "Id";
            this.workerId_1.Name = "workerId_1";
            // 
            // workerName_1
            // 
            this.workerName_1.HeaderText = "Name";
            this.workerName_1.Name = "workerName_1";
            // 
            // workerPay_1
            // 
            this.workerPay_1.HeaderText = "Wage";
            this.workerPay_1.Name = "workerPay_1";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(406, 280);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Sorted list of workers";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.workerId_2,
            this.workerName_2,
            this.workerPay_2});
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 3);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(400, 274);
            this.dataGridView2.TabIndex = 0;
            // 
            // workerId_2
            // 
            this.workerId_2.HeaderText = "Id";
            this.workerId_2.Name = "workerId_2";
            // 
            // workerName_2
            // 
            this.workerName_2.HeaderText = "Name";
            this.workerName_2.Name = "workerName_2";
            // 
            // workerPay_2
            // 
            this.workerPay_2.HeaderText = "Wage";
            this.workerPay_2.Name = "workerPay_2";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.listBox1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(406, 280);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Top 5 names";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(3, 3);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(400, 274);
            this.listBox1.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.listBox2);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(406, 280);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Bottom 3 Id";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // listBox2
            // 
            this.listBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(3, 3);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(400, 274);
            this.listBox2.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.dataGridView3);
            this.tabPage5.Controls.Add(this.showParseWorkers_Button);
            this.tabPage5.Controls.Add(this.read_Button);
            this.tabPage5.Controls.Add(this.write_Button);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(406, 280);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Wr/R";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.workerId_3,
            this.workerName_3,
            this.workerPay_3});
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridView3.Location = new System.Drawing.Point(3, 35);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(400, 242);
            this.dataGridView3.TabIndex = 3;
            // 
            // workerId_3
            // 
            this.workerId_3.HeaderText = "Id";
            this.workerId_3.Name = "workerId_3";
            // 
            // workerName_3
            // 
            this.workerName_3.HeaderText = "Name";
            this.workerName_3.Name = "workerName_3";
            // 
            // workerPay_3
            // 
            this.workerPay_3.HeaderText = "Wage";
            this.workerPay_3.Name = "workerPay_3";
            // 
            // showParseWorkers_Button
            // 
            this.showParseWorkers_Button.Location = new System.Drawing.Point(260, 6);
            this.showParseWorkers_Button.Name = "showParseWorkers_Button";
            this.showParseWorkers_Button.Size = new System.Drawing.Size(129, 23);
            this.showParseWorkers_Button.TabIndex = 2;
            this.showParseWorkers_Button.Text = "Show parse workers";
            this.showParseWorkers_Button.UseVisualStyleBackColor = true;
            this.showParseWorkers_Button.Click += new System.EventHandler(this.showParseWorkers_Button_Click);
            // 
            // read_Button
            // 
            this.read_Button.Location = new System.Drawing.Point(150, 6);
            this.read_Button.Name = "read_Button";
            this.read_Button.Size = new System.Drawing.Size(75, 23);
            this.read_Button.TabIndex = 1;
            this.read_Button.Text = "Read";
            this.read_Button.UseVisualStyleBackColor = true;
            this.read_Button.Click += new System.EventHandler(this.read_Button_Click);
            // 
            // write_Button
            // 
            this.write_Button.Location = new System.Drawing.Point(40, 6);
            this.write_Button.Name = "write_Button";
            this.write_Button.Size = new System.Drawing.Size(75, 23);
            this.write_Button.TabIndex = 0;
            this.write_Button.Text = "Write";
            this.write_Button.UseVisualStyleBackColor = true;
            this.write_Button.Click += new System.EventHandler(this.write_Button_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 306);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerId_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerName_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerPay_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerId_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerName_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerPay_2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button write_Button;
        private System.Windows.Forms.Button read_Button;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerId_3;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerName_3;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerPay_3;
        private System.Windows.Forms.Button showParseWorkers_Button;
    }
}

