﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;


namespace WindowsFormsApplication4
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Переменная для формирования псевдослучайных чисел
        /// </summary>
        static Random rnd;

        /// <summary>
        /// Массив имен для последующего их присвоения работникам
        /// </summary>
        string[] nameMass;

        /// <summary>
        /// Коллекция работников
        /// </summary>
        List<Workers> workers;

        /// <summary>
        /// Коллекция работников считанная из текстового документа
        /// </summary>
        List<Workers> parseWorkers;

        /// <summary>
        /// Имя текстового документа
        /// </summary>
        private string pathToTextFile = "WinForm4.txt";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            rnd = new Random();

            nameMass = new string[]         {  "Алексей", "Александр", "Вадим", "Владимир", "Валентин", "Денис", "Егор", "Кирилл",
                                                "Леонид", "Максим", "Матвей", "Никита", "Олег", "Павел", "Роман", "Алёна", "Анастасия",
                                                "Анна", "Вероника", "Виктория", "Екатерина", "Елена", "Ксения", "Марина", "Мария",
                                                "Надежда", "Наталья", "Нина", "Оксана", "Ольга", "Светлана", "Юлия", "Александра",
                                                "Валерий", "Валерия" };

            workers = new List<Workers>();

            parseWorkers = new List<Workers>();

            CreateWorkers(workers, rnd.Next(50, 150));

            ShowAllWorkersInfo(workers, dataGridView1);

            workers.Sort();

            ShowAllWorkersInfo(workers, dataGridView2);

            ShowSomeWorkersInfo(workers, listBox1, "Name", 0, 4);

            ShowSomeWorkersInfo(workers, listBox2, "Id", workers.Count - 3, workers.Count-1);
        }

        /// <summary>
        /// Метод наполнения коллекции работниками с указанием их ставкии, идентификатора, имени и рассчета его среднемесячого заработока
        /// </summary>
        /// <param name="number"></param>
        private void CreateWorkers(List<Workers> workers, int number)
        {        
            for (int i = 0; i < number; i++)
            {
                workers.Add(new FixedWageWorkers(rnd.Next(20000, 80000)));
                workers.Add(new HourlyWageWorkers(rnd.Next(120, 400)));
            }

            for (int i = 0; i < workers.Count; i++)
            {
                workers[i].Id = i;
                workers[i].Name = nameMass[rnd.Next(nameMass.Length)];
                workers[i].AverageMonthlyWage = workers[i].WageCalculate();
            }
        }

        /// <summary>
        /// Метод, позволяющий выводить всю информацию о работниках в dataGridView
        /// </summary>
        /// <param name="workers"></param>
        /// <param name="dataGridView"></param>
        private void ShowAllWorkersInfo(List<Workers> workers, DataGridView dataGridView)
        {
            for (int i = 0; i < workers.Count; i++)
            {
                dataGridView.Rows.Add();
                dataGridView.Rows[i].Cells[dataGridView.Columns[0].Name].Value = workers[i].Id;
                dataGridView.Rows[i].Cells[dataGridView.Columns[1].Name].Value = workers[i].Name;
                dataGridView.Rows[i].Cells[dataGridView.Columns[2].Name].Value = workers[i].AverageMonthlyWage;
            }
        }

        /// <summary>
        /// Метод, позвояющий выводить частичную информацию о работниках в ListBox
        /// </summary>
        /// <param name="workers"></param>
        /// <param name="listBox"></param>
        /// <param name="columnText"></param>
        /// <param name="startOne"></param>
        /// <param name="finishOne"></param>
        private void ShowSomeWorkersInfo(List<Workers> workers, ListBox listBox, string columnText, int startOne, int finishOne)
        {
            if (columnText == "Id")
            {
                for (int i = startOne; i <= finishOne; i++)
                {
                    listBox.Items.Add(workers[i].Id);
                }
            }

            if (columnText == "Name")
            {
                for (int i = startOne; i <= finishOne; i++)
                {
                    listBox.Items.Add(workers[i].Name);
                }
            }
        }

        /// <summary>
        /// Обработчик кнопки write_Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void write_Button_Click(object sender, EventArgs e)
        {
            FileProcessing FP = new FileProcessing();
            FP.WriteToFile(workers, pathToTextFile);
        }

        /// <summary>
        /// Обработчик кнопки read_Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void read_Button_Click(object sender, EventArgs e)
        {
            FileProcessing FP = new FileProcessing();
            FP.ReadFromFile(ref parseWorkers, ref pathToTextFile);
        }

        /// <summary>
        /// Обработчик кнопки showParseWorkers_Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void showParseWorkers_Button_Click(object sender, EventArgs e)
        {
           ShowAllWorkersInfo(parseWorkers, dataGridView3);           
        }    
    }
}