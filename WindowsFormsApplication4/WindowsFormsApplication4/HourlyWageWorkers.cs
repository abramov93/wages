﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication4
{
    /// <summary>
    /// Класс, описывающий информацию о работнике с почасовой оплатой
    /// </summary>
    public class HourlyWageWorkers : Workers
    {
        /// <summary>
        /// Среднее количество рабочих дней (в задании)
        /// </summary>
        const double CONST1 = 20.8;

        /// <summary>
        /// Количество рабочих часов в день (в задании)
        /// </summary>
        const double CONST2 = 8;

        /// <summary>
        /// Почасовая ставка работника
        /// </summary>
        double HourlyRate;

        public HourlyWageWorkers() { }

        public HourlyWageWorkers(double HourlyRate)
        {
            this.HourlyRate = HourlyRate;
        }

        public override double WageCalculate()
        {
            return CONST1 * CONST2 * HourlyRate;
        }
    }
}
