﻿using System;

namespace WindowsFormsApplication4
{
    /// <summary>
    /// Класс, описывающий информацию о работнике
    /// </summary>
    public abstract class Workers : IComparable<Workers>
    {
        /// <summary>
        /// Идентификатор работника
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя работника
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Среднемесячная заработная плата работника
        /// </summary>
        public double AverageMonthlyWage { get; set; }

        /// <summary>
        /// Метод, определяющий среднемесячный заработок работника
        /// </summary>
        public abstract double WageCalculate();

        /// <summary>
        /// Реализация интерфейса IComparable<Workers>
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(Workers other)
        {          
            int length; // Количество букв в наименее длинном имени из двух сравниваемых

            if (this.AverageMonthlyWage < other.AverageMonthlyWage)
                return 1;

            if (this.AverageMonthlyWage > other.AverageMonthlyWage)
                return -1;
            else
            {
                // Определяем количество букв в наименее длинном имени из двух сравниваемых
                if (this.Name.Length < other.Name.Length)
                    length = this.Name.Length;
                else
                    length = other.Name.Length;

                // Сортировка имен в алфавитном порядке
                for (int i = 0; i < length; i++)
                {
                    if (this.Name[i] > other.Name[i])   // Сравниваем побуквенно слова
                        return 1;

                    if (this.Name[i] < other.Name[i])   // Сравниваем побуквенно слова
                        return -1;
                    else
                    {               
                        if (i == length - 1)    // Если буквы наименьшего слова совпадают со всеми первыми буквами наибольшего слова, то сортируем по количеству символов (Александр, Александра)
                        {
                            if (this.Name.Length > other.Name.Length) // Если длина слова меньше длины проверяемого слова
                                return 1;

                            if (this.Name.Length < other.Name.Length) // Если длина слова больше длины проверяемого слова
                                return -1;

                            if (this.Name.Length == other.Name.Length) // Если длина слова равна длине проверяемого слова
                                return 0;

                            return 1;
                        }
                    }
                }
                return 0;
            }
        }
    }
}





